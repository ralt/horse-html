(defpackage :horse-html/tests
  (:use :common-lisp :fiveam :horse-html))

(in-package :horse-html/tests)

(defun add-package-alias (package alias)
  (rename-package package package (list alias)))

(add-package-alias :horse-html :hh)
