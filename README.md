# horse-html

horse-html is a ParenScript extension to generate HTML inside your
ParenScript code. This typically replaces your usage of
`PS:WHO-PS-HTML`.

My main gripe with `WHO-PS-HTML` is that it generates strings, not DOM
elements, which means that you must render it before you are able to
do anything with it.

For example:

```lisp
(use-package :parenscript)

(defun foo ()
  (who-ps-html (:div "foo")))
```

Will generate the following JavaScript code:

```javascript
function foo() {
    return ['<DIV>', 'foo', '</DIV>'];
}
```

With horse-html, you would use:

```lisp
(use-package :horse-html)

(defun foo ()
  (with-html (:div "foo")))
```

Which will generate the following JavaScript code:

```javascript
function foo() {
    var fragment = document.createDocumentFragment();
    var element = document.createElement('div');
    fragment.appendChild(element);
    element.innerText = 'foo';
    return fragment;
}
```

Some advantages coming out of this approach:

- You don't have to use `innerHTML`, which is long-deprecated.
- You can attach event listeners on the element wherever you are
  defining it.

The 2nd point means that you can write your applications in a similar
way as what react encourages with JSX, except closer to native.

For example, assuming this HTML:

```html
<div id="foo"></div>
```

And this ParenScript:

```lisp
(use-package :parenscript)
(use-package :horse-html)

(defun main ()
  (render (chain document (get-element-by-id "foo"))))
  
(defun render (el)
  (chain el (replace-with (render-foo el))))
  
(defun render-foo (el)
  (with-html (:div
                :onclick (lambda ()
                           (chain console (log "foo"))
                           (render el))
                "hi!")))
```

You can already see how passing state is done, and _it works_. Because
closures are kept, no string-attribute is generated there.

For convenience, horse-html supports exotic elements being
returned. For example, you can call a function:

```lisp
(with-html (:div (foo)))

(defun foo ()
  (with-html
    (:div :class "my-class" "foo!")
    (:div :class "other-class" "bar!")))
```

Or return a list:

```lisp
(with-html
  (:div (chain '(1 2)
               (map (lambda (i) (with-html (:div i)))))))
```

### The TODO app

Of course, to demonstrate what sort of code horse-html was built for,
the eternal TODO app has been rewritten :)

It resides in [todo.paren](todo.paren), with the HTML provided [in
there](todo.html). The HTML assumes you're using
[trident-mode](https://github.com/johnmastro/trident-mode.el).

If you're not using trident-mode, you will need to load the
[todo.js](todo.js) file which has been generated from the todo.paren
file.

### License

MIT license.
