(in-package :horse-html/tests)

(test one-tag
  (is
   (equal
    (hh::render-forms '((:div)))
    '(let ((hh::fragment (ps:chain hh::document
				   (hh::create-document-fragment))))
      (let ((hh::element (ps:chain hh::document
				   (hh::create-element "div"))))
	(ps:chain hh::fragment (hh::append-child hh::element)))
      hh::fragment))))

(test multiple-tags
  (is
   (equal
    (hh::render-forms '((:div) (:div) (:div)))
    '(let ((hh::fragment (ps:chain hh::document
				   (hh::create-document-fragment))))
      (let ((hh::element (ps:chain hh::document
				   (hh::create-element "div"))))
	(ps:chain hh::fragment (hh::append-child hh::element)))
      (let ((hh::element (ps:chain hh::document
				   (hh::create-element "div"))))
	(ps:chain hh::fragment (hh::append-child hh::element)))
      (let ((hh::element (ps:chain hh::document
				   (hh::create-element "div"))))
	(ps:chain hh::fragment (hh::append-child hh::element)))
      hh::fragment))))

(test content-string
  (is
   (equal
    (hh::render-forms '((:div "foo")))
    '(let ((hh::fragment (ps:chain hh::document
				   (hh::create-document-fragment))))
      (let ((hh::element (ps:chain hh::document
				   (hh::create-element "div"))))
	(ps:chain hh::fragment (hh::append-child hh::element))
	(setf (ps:@ hh::element hh::inner-text) "foo"))
      hh::fragment))))

(test content-int
  (is
   (equal
    (hh::render-forms '((:div 1)))
    '(let ((hh::fragment (ps:chain hh::document
				   (hh::create-document-fragment))))
      (let ((hh::element (ps:chain hh::document
				   (hh::create-element "div"))))
	(ps:chain hh::fragment (hh::append-child hh::element))
	(setf (ps:@ hh::element hh::inner-text) 1))
      hh::fragment))))

(test content-float
  (is
   (equal
    (hh::render-forms '((:div 1.0)))
    '(let ((hh::fragment (ps:chain hh::document
				   (hh::create-document-fragment))))
      (let ((hh::element (ps:chain hh::document
				   (hh::create-element "div"))))
	(ps:chain hh::fragment (hh::append-child hh::element))
	(setf (ps:@ hh::element hh::inner-text) 1.0))
      hh::fragment))))

(test content-element
  (is
   (equal
    (hh::render-forms '((:div (:div))))
    '(let ((hh::fragment (ps:chain hh::document
				   (hh::create-document-fragment))))
      (let ((hh::element (ps:chain hh::document
				   (hh::create-element "div"))))
	(ps:chain hh::fragment (hh::append-child hh::element))
	(let ((hh::child
	       (funcall
		(lambda ()
		  (let ((hh::fragment
			 (ps:chain hh::document
				   (hh::create-document-fragment))))
		    (let ((hh::element
			   (ps:chain hh::document
				     (hh::create-element "div"))))
		      (ps:chain hh::fragment
				(hh::append-child hh::element)))
		    hh::fragment)))))
	  (cond ((ps:chain hh::-array (hh::is-array hh::child))
		 (dolist (hh::sub-child hh::child)
		   (if (ps:instanceof hh::sub-child hh::-node)
		       (ps:chain hh::element
				 (hh::append-child hh::sub-child))
		       (setf (ps:@ hh::element hh::inner-text)
			     hh::sub-child))))
		((ps:instanceof hh::child hh::-node)
		 (ps:chain hh::element (hh::append-child hh::child)))
		(t
		 (setf (ps:@ hh::element hh::inner-text)
		       hh::child)))))
      hh::fragment))))

(test content-symbol
  (is
   (equal
    (hh::render-forms '((:div i)))
    '(let ((hh::fragment (ps:chain hh::document
				   (hh::create-document-fragment))))
      (let ((hh::element (ps:chain hh::document
				   (hh::create-element "div"))))
	(ps:chain hh::fragment (hh::append-child hh::element))
	(let ((hh::child i))
	  (cond ((ps:chain hh::-array (hh::is-array hh::child))
		 (dolist (hh::sub-child hh::child)
		   (if (ps:instanceof hh::sub-child hh::-node)
		       (ps:chain hh::element
				 (hh::append-child hh::sub-child))
		       (setf (ps:@ hh::element hh::inner-text)
			     hh::sub-child))))
		((ps:instanceof hh::child hh::-node)
		 (ps:chain hh::element (hh::append-child hh::child)))
		(t
		 (setf (ps:@ hh::element hh::inner-text)
		       hh::child)))))
      hh::fragment))))

(test content-function
  (is
   (equal
    (hh::render-forms '((:div (foo))))
    '(let ((hh::fragment (ps:chain hh::document
				   (hh::create-document-fragment))))
      (let ((hh::element (ps:chain hh::document
				   (hh::create-element "div"))))
	(ps:chain hh::fragment (hh::append-child hh::element))
	(let ((hh::child (foo)))
	  (cond ((ps:chain hh::-array (hh::is-array hh::child))
		 (dolist (hh::sub-child hh::child)
		   (if (ps:instanceof hh::sub-child hh::-node)
		       (ps:chain hh::element
				 (hh::append-child hh::sub-child))
		       (setf (ps:@ hh::element hh::inner-text)
			     hh::sub-child))))
		((ps:instanceof hh::child hh::-node)
		 (ps:chain hh::element (hh::append-child hh::child)))
		(t
		 (setf (ps:@ hh::element hh::inner-text)
		       hh::child)))))
      hh::fragment))))

(test content-lambda
  (is
   (equal
    (hh::render-forms
     '((:div
	(ps:chain '(1 2)
	 (map (lambda () (with-html (:div "bar"))))))))
    '(let ((hh::fragment (ps:chain hh::document
				   (hh::create-document-fragment))))
      (let ((hh::element (ps:chain hh::document
				   (hh::create-element "div"))))
	(ps:chain hh::fragment (hh::append-child hh::element))
	(let
	    ((hh::child
	      (ps:chain
	       '(1 2)
	       (map
		(lambda ()
		  (with-html (:div "bar")))))))
	  (cond ((ps:chain hh::-array (hh::is-array hh::child))
		 (dolist (hh::sub-child hh::child)
		   (if (ps:instanceof hh::sub-child hh::-node)
		       (ps:chain hh::element
				 (hh::append-child hh::sub-child))
		       (setf (ps:@ hh::element hh::inner-text)
			     hh::sub-child))))
		((ps:instanceof hh::child hh::-node)
		 (ps:chain hh::element (hh::append-child hh::child)))
		(t
		 (setf (ps:@ hh::element hh::inner-text)
		       hh::child)))))
      hh::fragment))))

(test attribute
  (is
   (equal
    (hh::render-forms '((:div :id "foo")))
    '(let ((hh::fragment (ps:chain hh::document
				   (hh::create-document-fragment))))
      (let ((hh::element (ps:chain hh::document
				   (hh::create-element "div"))))
	(ps:chain hh::fragment (hh::append-child hh::element))
	(setf (ps:getprop hh::element "id") "foo"))
      hh::fragment))))

(test attribute-and-content
  (is
   (equal
    (horse-html::render-forms '((:div :id "foo" "bar")))
    '(let ((hh::fragment (ps:chain hh::document
				   (hh::create-document-fragment))))
      (let ((hh::element (ps:chain hh::document
				   (hh::create-element "div"))))
	(ps:chain hh::fragment (hh::append-child hh::element))
	(setf (ps:getprop hh::element "id") "foo")
	(setf (ps:@ hh::element hh::inner-text) "bar"))
      hh::fragment))))

(test attribute-class
  (is
   (equal
    (hh::render-forms '((:div :class "foo")))
    '(let ((hh::fragment (ps:chain hh::document
				   (hh::create-document-fragment))))
      (let ((hh::element (ps:chain hh::document
				   (hh::create-element "div"))))
	(ps:chain hh::fragment (hh::append-child hh::element))
	(setf (ps:getprop hh::element "className") "foo"))
      hh::fragment))))

(test multiple-attributes-and-content
  (is
   (equal
    (hh::render-forms '((:div :id "foo" :class "bar" "baz")))
    '(let ((hh::fragment (ps:chain hh::document
				   (hh::create-document-fragment))))
      (let ((hh::element (ps:chain hh::document
				   (hh::create-element "div"))))
	(ps:chain hh::fragment (hh::append-child hh::element))
	(setf (ps:getprop hh::element "id") "foo")
	(setf (ps:getprop hh::element "className") "bar")
	(setf (ps:@ hh::element hh::inner-text) "baz"))
      hh::fragment))))

(test attribute-lambda
  (is
   (equal
    (hh::render-forms
     '((:div
	:onclick (lambda ()
		   (ps:chain console (log "foo"))))))
    '(let ((hh::fragment (ps:chain hh::document
				   (hh::create-document-fragment))))
      (let ((hh::element (ps:chain hh::document
				   (hh::create-element "div"))))
	(ps:chain hh::fragment (hh::append-child hh::element))
	(setf (ps:getprop hh::element "onclick")
	      (lambda () (ps:chain console (log "foo")))))
      hh::fragment))))

(test attribute-and-element-content
  (is
   (equal
    (hh::render-forms
     '((:div
	:id "foo"
	:class "bar"
	(:span :class "baz" "qux"))))
    '(let ((hh::fragment (ps:chain hh::document
				   (hh::create-document-fragment))))
      (let ((hh::element (ps:chain hh::document
				   (hh::create-element "div"))))
	(ps:chain hh::fragment (hh::append-child hh::element))
	(setf (ps:getprop hh::element "id") "foo")
	(setf (ps:getprop hh::element "className") "bar")
	(let ((hh::child
	       (funcall
		(lambda ()
		  (let ((hh::fragment
			 (ps:chain hh::document
				   (hh::create-document-fragment))))
		    (let ((hh::element
			   (ps:chain hh::document
				     (hh::create-element "span"))))
		      (ps:chain hh::fragment
				(hh::append-child hh::element))
		      (setf (ps:getprop hh::element "className")
			    "baz")
		      (setf (ps:@ hh::element hh::inner-text) "qux"))
		    hh::fragment)))))
	  (cond ((ps:chain hh::-array (hh::is-array hh::child))
		 (dolist (hh::sub-child hh::child)
		   (if (ps:instanceof hh::sub-child hh::-node)
		       (ps:chain hh::element
				 (hh::append-child hh::sub-child))
		       (setf (ps:@ hh::element hh::inner-text)
			     hh::sub-child))))
		((ps:instanceof hh::child hh::-node)
		 (ps:chain hh::element (hh::append-child hh::child)))
		(t
		 (setf (ps:@ hh::element hh::inner-text)
		       hh::child)))))
      hh::fragment))))

(test several-primitives
  (is
   (equal
    (hh::render-forms
     '((:div "foo" "bar")))
    '(let ((hh::fragment (ps:chain hh::document (hh::create-document-fragment))))
      (let ((hh::element (ps:chain hh::document (hh::create-element "div"))))
	(ps:chain hh::fragment (hh::append-child hh::element))
	(setf (ps:@ hh::element hh::inner-text) "foo")
	(setf (ps:@ hh::element hh::inner-text) "bar"))
      hh::fragment))))

(test several-elements-in-element
  (is
   (equal
    (hh::render-forms
     '((:div (:div) (:div))))
    '(let ((hh::fragment
	    (chain hh::document (hh::create-document-fragment))))
      (let ((hh::element
	     (chain hh::document (hh::create-element "div"))))
	(chain hh::fragment (hh::append-child hh::element))
	(let ((hh::child
	       (funcall
		(lambda ()
		  (let ((hh::fragment
			 (chain hh::document
				(hh::create-document-fragment))))
		    (let ((hh::element
			   (chain hh::document
				  (hh::create-element "div"))))
		      (chain hh::fragment
			     (hh::append-child hh::element)))
		    hh::fragment)))))
	  (cond
	    ((chain hh::-array (hh::is-array hh::child))
	     (dolist (hh::sub-child hh::child)
	       (if (instanceof hh::sub-child hh::-node)
		   (chain hh::element
			  (hh::append-child hh::sub-child))
		   (setf (@ hh::element hh::inner-text)
			 hh::sub-child))))
	    ((instanceof hh::child hh::-node)
	     (chain hh::element
		    (hh::append-child hh::child)))
	    (t
	     (setf (@ hh::element hh::inner-text)
		   hh::child))))
	(let ((hh::child
	       (funcall
		(lambda ()
		  (let ((hh::fragment
			 (chain hh::document
				(hh::create-document-fragment))))
		    (let ((hh::element
			   (chain hh::document
				  (hh::create-element "div"))))
		      (chain hh::fragment
			     (hh::append-child hh::element)))
		    hh::fragment)))))
	  (cond
	    ((chain hh::-array (hh::is-array hh::child))
	     (dolist (hh::sub-child hh::child)
	       (if (instanceof hh::sub-child hh::-node)
		   (chain hh::element
			  (hh::append-child hh::sub-child))
		   (setf (@ hh::element hh::inner-text)
			 hh::sub-child))))
	    ((instanceof hh::child hh::-node)
	     (chain hh::element
		    (hh::append-child hh::child)))
	    (t
	     (setf (@ hh::element hh::inner-text)
		   hh::child)))))
      hh::fragment))))
