function startTasksApp(root) {
    var tasks = [{ 'checked' : null, 'text' : 'foo' }, { 'checked' : null, 'text' : 'bar' }];
    var ctx = { 'root' : root, 'tasks' : tasks };
    __PS_MV_REG = [];
    return renderTasks(ctx);
};
function clearChildren(root) {
    return root.innerHTML = '';
};
function renderTasks(ctx) {
    var root = ctx['root'];
    var tasks = ctx['tasks'];
    clearChildren(root);
    tasks.map(function (task) {
        __PS_MV_REG = [];
        return root.appendChild(renderTask(ctx, task));
    });
    __PS_MV_REG = [];
    return root.appendChild(renderTaskAddForm(ctx));
};
function renderTask(ctx, task) {
    var fragment = document.createDocumentFragment();
    var element = document.createElement('div');
    fragment.appendChild(element);
    element['className'] = 'task';
    var child209 = (function () {
        var fragment210 = document.createDocumentFragment();
        var element211 = document.createElement('div');
        fragment210.appendChild(element211);
        element211['onclick'] = function (e) {
            e.preventDefault();
            task['checked'] = !task['checked'];
            __PS_MV_REG = [];
            return renderTasks(ctx);
        };
        var child = (function () {
            var fragment212 = document.createDocumentFragment();
            var element213 = document.createElement('input');
            fragment212.appendChild(element213);
            element213['checked'] = task['checked'];
            element213['type'] = 'checkbox';
            return fragment212;
        })();
        if (Array.isArray(child)) {
            for (var subChild = null, _js_idx214 = 0; _js_idx214 < child.length; _js_idx214 += 1) {
                subChild = child[_js_idx214];
                if ((subChild instanceof Node)) {
                    element211.appendChild(subChild);
                } else {
                    element211.innerText = subChild;
                };
            };
        } else if ((child instanceof Node)) {
            element211.appendChild(child);
        } else {
            element211.innerText = child;
        };
        var child215 = (function () {
            var fragment216 = document.createDocumentFragment();
            var element217 = document.createElement('span');
            fragment216.appendChild(element217);
            var child = task['text'];
            if (Array.isArray(child)) {
                for (var subChild = null, _js_idx218 = 0; _js_idx218 < child.length; _js_idx218 += 1) {
                    subChild = child[_js_idx218];
                    if ((subChild instanceof Node)) {
                        element217.appendChild(subChild);
                    } else {
                        element217.innerText = subChild;
                    };
                };
            } else if ((child instanceof Node)) {
                element217.appendChild(child);
            } else {
                element217.innerText = child;
            };
            return fragment216;
        })();
        if (Array.isArray(child215)) {
            for (var subChild = null, _js_idx219 = 0; _js_idx219 < child215.length; _js_idx219 += 1) {
                subChild = child215[_js_idx219];
                if ((subChild instanceof Node)) {
                    element211.appendChild(subChild);
                } else {
                    element211.innerText = subChild;
                };
            };
        } else if ((child215 instanceof Node)) {
            element211.appendChild(child215);
        } else {
            element211.innerText = child215;
        };
        return fragment210;
    })();
    if (Array.isArray(child209)) {
        for (var subChild = null, _js_idx220 = 0; _js_idx220 < child209.length; _js_idx220 += 1) {
            subChild = child209[_js_idx220];
            if ((subChild instanceof Node)) {
                element.appendChild(subChild);
            } else {
                element.innerText = subChild;
            };
        };
    } else if ((child209 instanceof Node)) {
        element.appendChild(child209);
    } else {
        element.innerText = child209;
    };
    var child = (function () {
        var fragment221 = document.createDocumentFragment();
        var element222 = document.createElement('div');
        fragment221.appendChild(element222);
        element222['className'] = 'delete';
        element222['onclick'] = function () {
            var tasks = ctx['tasks'];
            tasks.splice(tasks.indexOf(task));
            __PS_MV_REG = [];
            return renderTasks(ctx);
        };
        element222.innerText = 'click to delete';
        return fragment221;
    })();
    if (Array.isArray(child)) {
        for (var subChild = null, _js_idx223 = 0; _js_idx223 < child.length; _js_idx223 += 1) {
            subChild = child[_js_idx223];
            if ((subChild instanceof Node)) {
                element.appendChild(subChild);
            } else {
                element.innerText = subChild;
            };
        };
    } else if ((child instanceof Node)) {
        element.appendChild(child);
    } else {
        element.innerText = child;
    };
    return fragment;
};
function renderTaskAddForm(ctx) {
    var fragment = document.createDocumentFragment();
    var element = document.createElement('form');
    fragment.appendChild(element);
    element['onsubmit'] = function () {
        var newTask = this.elements['task'].value;
        ctx['tasks'].push({ 'checked' : null, 'text' : newTask });
        __PS_MV_REG = [];
        return renderTasks(ctx);
    };
    var child = (function () {
        var fragment224 = document.createDocumentFragment();
        var element225 = document.createElement('input');
        fragment224.appendChild(element225);
        element225['type'] = 'text';
        element225['name'] = 'task';
        return fragment224;
    })();
    if (Array.isArray(child)) {
        for (var subChild = null, _js_idx226 = 0; _js_idx226 < child.length; _js_idx226 += 1) {
            subChild = child[_js_idx226];
            if ((subChild instanceof Node)) {
                element.appendChild(subChild);
            } else {
                element.innerText = subChild;
            };
        };
    } else if ((child instanceof Node)) {
        element.appendChild(child);
    } else {
        element.innerText = child;
    };
    var child227 = (function () {
        var fragment228 = document.createDocumentFragment();
        var element229 = document.createElement('input');
        fragment228.appendChild(element229);
        element229['type'] = 'submit';
        element229['value'] = 'add';
        return fragment228;
    })();
    if (Array.isArray(child227)) {
        for (var subChild = null, _js_idx230 = 0; _js_idx230 < child227.length; _js_idx230 += 1) {
            subChild = child227[_js_idx230];
            if ((subChild instanceof Node)) {
                element.appendChild(subChild);
            } else {
                element.innerText = subChild;
            };
        };
    } else if ((child227 instanceof Node)) {
        element.appendChild(child227);
    } else {
        element.innerText = child227;
    };
    return fragment;
};
function main() {
    var el = document.getElementById('app');
    __PS_MV_REG = [];
    return startTasksApp(el);
};
main();
