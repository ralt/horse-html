(defsystem "horse-html"
  :license "MIT"
  :author "Florian Margaine <florian@margaine.com>"
  :description "Parenscript/HTML done better"
  :depends-on ("parenscript")
  :components ((:file "package")
	       (:file "horse"))
  :in-order-to ((test-op (test-op "horse-html/tests"))))

(defsystem "horse-html/tests"
  :depends-on ("horse-html" "fiveam")
  :components ((:file "package-tests")
	       (:file "tests"))
  :perform (test-op (o c) (symbol-call :fiveam '#:run-all-tests)))
