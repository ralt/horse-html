(in-package :horse-html)

(defmacro defreplacement (to-replace replacement)
  `(setf (gethash ,to-replace *replacements*) ,replacement))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defvar *replacements* (make-hash-table :test #'equal))

  (defreplacement "class" "className")

  (defun has-attributes (form)
    (> (length form) 1))

  (defun render-attributes (form)
    (let ((pairs (if (evenp (length form)) form (butlast form))))
      (loop for (k v) on pairs by #'cddr
	 when (keywordp k)
	 collect `(setf (ps:getprop element
				    ,(let ((attr-name
					    (string-downcase
					     (symbol-name k))))
				       (or (gethash attr-name *replacements*)
					   attr-name)))
			,v))))

  (defun has-child (form)
    ;; symbols are "unknown at build time" variables, so they
    ;; need to be treated as a child.
    (or (symbolp (first form))
	(listp (first form))))

  (defun render-child (form)
    (list
     `(let ((child ,(if (and (listp (first form))
			     (keywordp (first (first form))))
			;; recurse
			`(funcall
			  (lambda ()
			    ,(render-forms (list (first form)))))
			;; funcall
			(first form))))
	(cond ((ps:chain -array (is-array child))
	       (dolist (sub-child child)
		 (if (ps:instanceof sub-child -node)
		     (ps:chain element (append-child sub-child))
		     (setf (ps:@ element inner-text) sub-child))))
	      ((ps:instanceof child -node)
	       (ps:chain element (append-child child)))
	      (t
	       (setf (ps:@ element inner-text) child))))))

  (defun has-primitive (form)
    (or
     (stringp (first form))
     (realp (first form))))

  (defun render-primitive (form)
    (list `(setf (ps:@ element inner-text) ,(first form))))

  (defun render-form (form)
    `(let ((element (ps:chain document
			      (create-element
			       ,(string-downcase
				 (symbol-name (first form)))))))
       (ps:chain fragment (append-child element))
       ,@(let ((result))
	   (setf form (rest form))
	   (loop
	      until (eq form nil)
	      when (and form (has-attributes form))
	      do (let ((attrs (render-attributes form)))
		   (setf form (subseq form (* 2 (length attrs))))
		   (setf result (append result attrs)))
	      when (and form (has-primitive form))
	      do (progn
		   (setf result (append result (render-primitive form)))
		   (setf form (rest form)))
	      when (and form (has-child form))
	      do (progn
		   (setf result (append result (render-child form)))
		   (setf form (rest form))))
	  result)))

  (defun render-forms (forms)
    `(let ((fragment (ps:chain document (create-document-fragment))))
       ,@(mapcar #'render-form forms)
       fragment)))

(defmacro with-html (&rest forms)
  (render-forms forms))

(ps:import-macros-from-lisp 'with-html)
